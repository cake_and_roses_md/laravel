<html lang="fr">
	<head>
		<meta charset="UTF-8" />
		<title>Administration | Cake&Roses</title>
		<link rel="stylesheet" href="{{asset('css/style_admin.css')}}" />
	</head>
	<body class="admin-body">
	<div class="row container">
			<div class="col-3">
				<nav class="administration--nav">
					<ul>
						<li class="admin-nav-link">
							<a href="{{ url('/ajout-article/') }}">Article</a>
						</li>
						<li class="admin-nav-link">
							<a href="{{ url('/ajout-tag/') }}">Tag</a>
						</li>
						<li class="admin-nav-link">
							<a href="{{ url('/liste/') }}">Eddition</a>
						</li>
						<li class="admin-nav-link">
							<a class='logout' href="{{ url('/logout') }}">Déconnexion</a>
						</li>
					</ul>
				</nav>
			</div>

        @yield('content')
		
	</body>
</html>