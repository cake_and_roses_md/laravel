<footer id="footer">
	@if (count($errors)>0)
	    <span>
	        <ul>
	            @foreach($errors->all() as $error)
	            <li class="error">{{$error}}</li>
	            @endforeach
	        </ul>
	    </span>
	@endif

	<form action="{{ url('contact') }}" method="post">
	  <input type="hidden" name="{{ csrf_token() }}" value="">
		  {{ csrf_field() }}

		<div class="{{ $errors->has('nom')}} left">
			<label>Nom :</label> <br />
			<input class="input_form" type="text" name="Wayne" placeholder="Wayne" value="{{old('nom')}}">
		</div>

		<div class="{{ $errors->has('prenom')}} right">
			<label>Prénom :</label> <br />
			<input class="input_form" type="text" name="Bruce" placeholder="Prénom" value="{{old('prenom')}}">
		</div>

		<div class="{{ $errors->has('mail')}} left">
			<label>Mail :</label> <br />
			<input class="input_form" type="mail" name="mail" placeholder="imbatman@gmail.com" value="{{old('mail')}}">
		</div>

		<div class="{{ $errors->has('sujet')}} right">
			<label>Sujet :</label> <br />
			<input class="input_form" type="text" name="sujet" placeholder="Je vous envoie ce mail pour ..." value="{{old('sujet')}}">
		</div>

		<div class="{{ $errors->has('message')}} message">
			<label>Message :</label> <br />
			<textarea type="text" name="Message" placeholder="GG est le plus beau" value="{{old('message')}}"></textarea>
		</div>

		<input class="btn_envoyer right" type="submit" value="Envoyer" name="envoyer">
	
	</form>
	<p> Laravel examen | Matthieu et Daniel | 2017  </p>
</footer>