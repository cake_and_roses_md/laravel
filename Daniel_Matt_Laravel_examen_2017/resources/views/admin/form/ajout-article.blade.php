@extends('layout/admin')
@section('content')

    @if (count($errors)>0)
        <span>
            <ul>
                @foreach($errors->all() as $error)
                <li>{{$error}}</li>
                @endforeach
            </ul>
        </span>
    @endif

	<form method="post" action="{{ url('ajout-article') }}" enctype="multipart/form-data">
    <input type="hidden" name="{{ csrf_token() }}" value="">{{ csrf_field() }}

		<h3>Ajouter un Article : </h3>

        <h2>Catégorie : </h2>
        <div class="{{ $errors->has('categorie')}}">
        <input name="artcategorie" checked="checked" type="radio" value="0">Cakes<br />
        <input name="artcategorie" type="radio" value="1">Roses
        </div>
        <div class="{{ $errors->has('artName')}}">
        <h2>Nom:</h2>
		<input type="text" name="artName" placeholder="Nom de l'article" />
</div>
        <div class="{{ $errors->has('artDescription')}}">
        <h2>Description:</h2>
		<textarea name="artDescription" placeholder="Description de l'article"></textarea>
        </div>

        <h2>Choisissez les tags correspondant : </h2>
				<?php 
				for ($i=1; $i < 6 ; $i++) { ?>
				   <input type="checkbox" name="tagName" value="Bike"> Tag <?php echo $i;?>
				<?php }?>

		<h2 class="h2_img">
			Ajouter une/des image(s) :
		</h2>

		<input type="file" name="imaName[]" multiple="true"/>
		<output id="list-show--up_img"></output>
        <script>
            function handleFileSelect(evt) {
                var files = evt.target.files;
                for (var i = 0, f; f = files[i]; i++) {
                    if (!f.type.match('image.*')) {
                        continue;
                    }

                    var reader = new FileReader();

                    reader.onload = (function(theFile) {
                        return function(e) {
                            var span = document.createElement('span');
                            span.innerHTML = ['<img class="thumb" src="', e.target.result,
                            '" title="', escape(theFile.name), '"/>'].join('');
                            document.getElementById('list-show--up_img').insertBefore(span, null);
                        };
                    })(f);
                    reader.readAsDataURL(f);
                }
            }
            document.getElementById('articleImage').addEventListener('change', handleFileSelect, false);
        </script>
<br /><br />
		<input type="submit" value="Ajouter l'article" />

	</form>
    
@endsection
