@extends('layout/admin')
@section('content')

	@if (count($errors)>0)
	    <span>
	        <ul>
	            @foreach($errors->all() as $error)
	            <li>{{$error}}</li>
	            @endforeach
	        </ul>
	    </span>
	@endif

<form method="post" action="{{ url('ajout-tag') }}" enctype="multipart/form-data">
		  <input type="hidden" name="{{ csrf_token() }}" value="">{{ csrf_field() }}

		<h3>Ajouter un Tag: </h3>

		<div class="{{ $errors->has('categorie')}}">
        	<h2>Catégorie : </h2>

        	<input name="categorie" checked="checked" type="radio" value="0">Cakes<br />
        	<input name="categorie" type="radio" value="1">Roses<br />

		</div>
		<div class="{{ $errors->has('tagName')}}">
        	<h2>Nom:</h2>

		  	<input type="text" name="tagName" placeholder="Nom du tag" value="{{old('tagName')}}" /><br /><br />

		</div>

	<input type="submit" value="Enregistrer le Tag" />

</form>

@endsection