@extends('layout/admin')
@section('content')

<div class="content-admin">
<h3>Liste des articles :</h3>
	<table>
		<thead >
			<tr class="row thead_list_article_admin">
				<td class='col-4 name_article_admin'>Nom</td>
				<td class='col-4 categorie_article_admin'>Catégorie</td>
				<td class='col-2'></td>
				<td class='col-2'></td>
			</tr>
		</thead>
		<tbody>
			<?php 
				for ($i=1; $i < 6 ; $i++) { ?>
			    <tr class="row">
					<td class="col-4">Nom article <?php echo $i;?></td>
					<td class="col-4">Cake</td>
					<td class="col-2"><a href="#" class="_btn editarticles">édition</a></td>
					<td class="col-2"><button class="_btn danger deletearticles">Supprimer</button></td>
			    </tr>
			<?php }?>
			    
		</tbody>
	</table>
	
	<h3>Liste des tags :</h3>
	
	<table>
		<thead >
			<tr class="row thead_list_article_admin">
				<td class='col-4 name_article_admin'>Nom</td>
				<td class='col-4 categorie_article_admin'>Catégorie</td>
				<td class='col-2'></td>
				<td class='col-2'></td>
			</tr>
		</thead>
		<tbody>
			<?php 
				for ($i=1; $i < 4 ; $i++) { ?>
			    <tr class="row">
					<td class="col-4">Nom tag <?php echo $i;?> </td>
					<td class="col-4">Roses</td>
					<td class="col-2"><a href="#" class="_btn editarticles">édition</a></td>
					<td class="col-2"><button class="_btn danger deletearticles">Supprimer</button></td>
			    </tr>
			 <?php }?>
		</tbody>
	</table>
	<script>
		$('button._btn.deletearticles').click(function() {
			let el = $(this);
			el.parent('td').parent('tr').children('td.delete-confirm').addClass('active')
		})
		$('button.cancelDelete._btn').click(function() {
			$(this).parent('.cancel').parent().parent('td.delete-confirm').removeClass('active')
		})
	</script>
	</div>
@endsection
	