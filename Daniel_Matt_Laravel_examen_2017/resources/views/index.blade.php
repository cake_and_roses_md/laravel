@extends('layout/master')

@section('content')

		<header id="header" class="slider">

			<div class="logo">Logo Cake and Roses</div>

			<a  href="#info" title="Bouton cake" class="cake">Cake</a>
			<a  href="#info" title="Bouton rose" class="roses">Roses</a>
			
		</header><!-- /header -->

		<section class="container">
			<a href="#" class="switch_categorie" title="changement de catégorie"=>changement de catégorie</a>
			<section class="info">
				<h1 id="info"> Qui sommes-nous ? </h1>
				<p> Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
				tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
				quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
				consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
				cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
				proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
			</section><!-- End Header -->

			<section class="produit">

				<h2> Nos fleurs | nos gâteaux</h2>

				<ul class="tag">
				<?php 
				for ($i=1; $i < 6 ; $i++) { ?>
					<li><a href="#" title="tag">TAG <?php echo $i;?></a></li>
				<?php
				}
				?>	
				</ul>

				<div class="img_produit">

					<div class="img_big">Ceci est la grosse img</div>

						
						
					
							<?php 
							for ($i=1; $i < 6 ; $i++) { ?>
								<div class="img_small">petite image N°<?php echo $i;?></div>
							<?php }?>
						
					</div>
				</div>


				<div class="description">

					<h2>Description</h2>

					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
					tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
					quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
					consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
					cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
					proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>

				</div>

			</section>
			
		</section>


@endsection
