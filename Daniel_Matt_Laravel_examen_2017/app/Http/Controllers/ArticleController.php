<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Post;
use Session;
use DB;

class ArticleController extends Controller {

    public function getArticle(){

        return view('index');
    }
    public function postArticle(Request $request){
        $this->validate($request, [
            'artName' => 'required',
            'artDescription' => 'required'
        ], [
            'artName.required' => 'Le nom de l\'article est obligatoire !',
            'artDescription.required' => 'La description est obligatoire !',
        ]
    );
        $data = array(
            $artcategorie = 'artcategorie' => $request->artcategorie,
            $artName = 'artName' => $request->artName,
            $artDescription = 'artDescription' => $request->artDescription,
            $tagName = 'tagName' => $request->tagName,
        );
         $artcategorie= $artcategorie*1;
     DB::insert('insert into article (artCategorie, artName, artDescription) values (?,?,?)', [$artcategorie, $artName, $artDescription]);
/*
        $this->validate(request(),[
            'name'=>'required',
        ]);
        $user = new file;

        $user->title= Input::get('name');
        if (Input::hasFile('image')) {
            $file=Input::file('image');
            $file->move(public_path('/img/cake'). '/', $file->getClientOriginalName());

            $user->name = $file->getClientOriginalName();
        }
        $user->save();*/

        return redirect('/ajout-article');
    }
}