<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Post;
use Session;
use DB;

class TagController extends Controller {

    public function getTag(){

        return view('index');
    }
    public function postTag(Request $request){
        $this->validate($request, [
            'tagName' => 'required|min:2|regex:([a-zA-Z]+ *)'
        ], [
            'tagName.required' => 'Le nom du Tag est obligatoire !',
            'tagName.regex' => 'Tag incorrect !',
            'tagName.min' => 'Le tag doit avoir plus de deux caractères'
        ]
    );
        $data = array(
            $categorie ='categorie' => $request->categorie,
            $tagName = 'tagName' => $request->tagName

        );
        $categorie= $categorie*1;
        DB::insert('insert into tag (tagName, tagCategorie) values (?, ?)', [$tagName, $categorie]);

        return redirect('/ajout-tag');
    }
}
