<?php

//index
Route::get('//', function () {
    return view('index');
});
Route::get('/index/', function () {
    return view('index');
});

//rose ou cake
Route::get('/cake/', function () {
    return view('index-cake');
});
Route::get('/index/cake/', function () {
    return view('index-cake');
});
Route::get('/rose/', function () {
    return view('index-rose');
});
Route::get('/index/rose/', function () {
    return view('index-rose');
});

//filtrage
Route::get('/cake/', function () {
    return view('index');
});
Route::get('/index/cake/', function () {
    return view('index');
});

//administration
Route::get('/admin/', function () {
    return view('admin');
});

//menu administration
Route::get('/ajout-tag/', function () {
    return view('admin/form/ajout-tag');
});
//envoi nouv tag
Route::post('ajout-tag/', 'TagController@postTag');

Route::get('/ajout-article/', function () {
    return view('admin/form/ajout-article');
});
//envoi nouv art
Route::post('ajout-article/', 'ArticleController@postArticle');

Route::get('/liste/', function () {
    return view('admin/form/list');
});

//envoi mail contact
Route::post('contact', 'MailController@postMail');

//deconnexion
Route::get('/logout/{id}', 'AdminController@getId');