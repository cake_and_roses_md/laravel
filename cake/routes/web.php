<?php

//index
Route::get('/', function () {
    return view('index');
});
Route::get('index/', function () {
    return view('index');
});

//rose ou cake
Route::get('cake/', function () {
    return view('index-cake');
});
Route::get('rose/', function () {
    return view('index-roses');
});

//affichage info index-cake
Route::get('cake/', 'IndexController@getCake');
//affichage info index-cake
Route::get('rose/', 'IndexController@getRoses');

//administration
Route::get('admin/', function () {
    return view('admin');
});

//menu administration
Route::get('ajout-tag/', function () {
    return view('admin/form/ajout-tag');
});

//envoi nouv tag
Route::post('ajout-tag/', 'TagController@postTag');
Route::get('ajout-tag/', 'TagController@getTag');

Route::get('ajout-article/', function () {
    return view('admin/form/ajout-article');
});

//envoi nouv art
Route::post('ajout-article/', 'ArticleController@postArticle');
Route::get('ajout-article/', 'ArticleController@getArticle');


Route::get('liste/', function () {
    return view('admin/form/list');
});
Route::get('liste/', 'ListeController@getListe');

//envoi mail contact
Route::post('contact', 'MailController@postMail');

//deconnexion
Route::get('/logout/{id}', 'AdminController@getId');