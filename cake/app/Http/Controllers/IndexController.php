<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Post;
use DB;
use URL;
use App\Models\Image;

class IndexController extends Controller
{

	public function getCake(request $request){

		// $img_rand = DB::select('select id,imaName from image where imaCategorie = :imaCategorie', ['imaCategorie' => 0])->inRandomOrder()->get();

		$img_rand = Image::inRandomOrder(1)->get();



		$tags = DB::select('select tagName from tag where tagCategorie = :tagCategorie', ['tagCategorie' => 0]);

		$img_cake = DB::select('select imaName, id_article from image where imaCategorie = :imaCategorie', ['imaCategorie' => 0]);

		$data = array(
            'tags' => $tags,
            'img_cake' => $img_cake,
            'img_rand' => $img_rand
        );

		return view('index-cake', $data);

	}

	public function getRoses(Request $request){

		$tags = DB::select('select tagName from tag where tagCategorie = :tagCategorie', ['tagCategorie' => 1]);
		
		$img_roses = DB::select('select imaName, id_article from image where imaCategorie = :imaCategorie', ['imaCategorie' => 1]);

		$data = array(
            'tags' => $tags,
            'img_roses' => $img_roses
        );

		return view('index-roses', $data);

	}

}

