<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Post;
use Mail;
use Session;

class MailController extends Controller {

    public function getMail(){

        return view('index');
    }


    public function postMail(Request $request){

        $this->validate($request, [
            'nom' => 'required|min:2|regex:([a-zA-Z]+ *)',
            'prenom' => 'required|min:2|regex:([a-zA-Z]+ *)',
            'mail' => 'required|mail',
            'mail' => 'required|min:10',            
            'message' => 'required|min:50'
        ], [
            'mail.required' => 'Mail obligatoire !',
            'nom.required' => 'Nom obligatoire !',
            'nom.regex' => 'Nom incorrect',
            'prenom.required' => 'Prénom obligatoire !',
            'prenom.regex' => 'Prénom incorrect',
            'message.required' => 'Message obligatoire !',
            'message.min' => 'Votre message est trop court ! (50 caractères minimum)',            
            'sujet.required' => 'Sujet obligatoire !',
            'sujet.min' => 'Sujet obligatoire ! (10 caractères minimum)'
        ]
    );

        $data = array(
            'nom' => $request->nom,
            'prenom' => $request->prenom,
            'mail' => $request->mail,
            'sujet' => $request->sujet,            
            'bodyMessage' => $request->message,

        );

        Mail::send('emails.contact', $data, function($message) use ($data){
            $message->from($data['mail']);
            $message->to('GGestunDieu@laposte.net');
        });

        return redirect('/#footer');
    }
}
