<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Post;
use Session;
use DB;
	
    class listeController extends Controller
    {
	public function getListe(request $request){

		$list_art = DB::select('select artName from article');
		$list_tag = DB::select('select tagName from tag');

		$data = array(
            'list_art' => $list_art,
            'list_tag' => $list_tag
        );
		return view('admin/form/list', $data);
	}
}