<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Post;
use Session;
use DB;

class ArticleController extends Controller {

    public function getArticle(request $request){

        $tags_cake = DB::select('select tagName from tag where tagCategorie = :tagCategorie', ['tagCategorie' => 0]);

        $tags_roses = DB::select('select tagName from tag where tagCategorie = :tagCategorie', ['tagCategorie' => 1]);
        
        $data = array(
            'tags_cake' => $tags_cake,
            'tags_roses' => $tags_roses 
        );

        return view('admin/form/ajout-article', $data);

    }

    public function postArticle(Request $request){
        $this->validate($request, [
            'artName' => 'required',
            'artDescription' => 'required'
        ], [
            'artName.required' => 'Le nom de l\'article est obligatoire !',
            'artDescription.required' => 'La description est obligatoire !',
        ]
    );
        $data = array(
            $artcategorie = 'artcategorie' => $request->artcategorie,
            $artName = 'artName' => $request->artName,
            $artDescription = 'artDescription' => $request->artDescription,
            $tagName = 'tagName' => $request->tagName,
        );
            $artcategorie = $request->artcategorie;
            $artName = $request->artName;
            $artDescription = $request->artDescription;

         $artcategorie= $artcategorie*1;
         
            DB::insert('insert into article (artCategorie, artName, artDescription) values (?,?,?)', [$artcategorie, $artName, $artDescription]);


            $article_new = DB::select('select id from article WHERE  artName = :artName AND artDescription = :artDescription ';

        $file = $request->file('imaName');
        $fileName = $suffix.'_'.rand(11111,99999).'.jpg';
        $destinationPath = base_path() . '/public/img/art/';
 
        $request->file('imaName')->move($destinationPath, $fileName);
 
            DB::insert('insert into image (imaName, id_article) values (?,?)', [$fileName, $article_new]);

/*
        $this->validate(request(),[
            'name'=>'required',
        ]);
        $user = new file;

<<<<<<< Updated upstream
            $fileName = $suffix.'_'.rand(11111,99999).'.jpg';
            $file=Input::files();
            $file->move(public_path('$destinationPath'). '/', $fileName);

            $image_art->name = $fileName;
            //$image_art->name = $file->getClientSize();
            //$image_art->name = $file->getClientMimeType();

            DB::insert('insert into image (imaName, id_article) values (?,?)', [$fileName, 1]);

        $image_art->save();
    }

//             $file = array('image' => Input::file('image'));
//             if ($artcategorie == 0){

//                 $destinationPath = 'public/img/cake';
//                 $suffix= "cake";

//                 }else{
//                     $destinationPath = 'public/img/roses';
//                     $suffix= "roses";
//             }
// //Image::make($image->getRealPath())->resize(200, 200)->save($path);

//               $extension = 'jpg'; // getting image extension
=======
        $user->title= Input::get('name');
        if (Input::hasFile('image')) {
            $file=Input::file('image');
            $file->move(public_path('/img/cake'). '/', $file->getClientOriginalName());
>>>>>>> Stashed changes

            $user->name = $file->getClientOriginalName();
        }
        $user->save();*/

        return redirect('/ajout-article');
    }
}