<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Post;
use Session;
use DB;

class TagController extends Controller {

    public function getTag(){
        
//         $tags_cake = DB::select('select tagName from tag where tagCategorie = :tagCategorie', ['tagCategorie' => 0]);

//         $tags_roses = DB::select('select tagName from tag where tagCategorie = :tagCategorie', ['tagCategorie' => 1]);

//         $data = array(
//             'tags_cake' => $tags_cake,
//             'tags_roses' => $tags_roses
//         );
 
//     $cpt_cake=0;
//     $cpt_roses=0;
 
// foreach ($tags_cake as $cacas => $value) {
//      $cpt_cake++ ;
// }
// foreach ($tags_roses as $caca => $value) {
//      $cpt_roses++ ;
// }


        return view('admin/form/ajout-tag');
    }

    public function postTag(Request $request){
        $this->validate($request, [
            'tagName' => 'required|min:2|regex:([a-zA-Z]+ *)'
        ], [
            'tagName.required' => 'Le nom du Tag est obligatoire !',
            'tagName.regex' => 'Tag incorrect !',
            'tagName.min' => 'Le tag doit avoir plus de deux caractères'
        ]
    );
        $data = array(
            'categorie' => $request->categorie,
            'tagName' => $request->tagName
        );
        $tagName = $request->tagName;
        $categorie = $request->categorie;
        $categorie= $categorie*1;

        DB::insert('insert into tag (tagName, tagCategorie) values (?, ?)', [$tagName, $categorie]);

        return redirect('/ajout-tag');
    }
}
