<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Image extends Model

{
protected $table ='image';
protected $primaryKey ='id';
protected $filable = ['imaName' , 'imaCategorie'];

public $timesstamp = false;
}