@extends('layout/master')

@section('content')

		<header id="header" class="slider">
<?php $cpt = rand(1,6);
 $cat = rand(1,2);

 if($cat==1){$nom='cake';}else{$nom='roses';}
 ?>
<img class="img_header"src="{{ URL::to ('img/'.$nom.'/'.$nom.'_'.$cpt.'.jpg') }}" alt="image header">

			<div class="logo">Logo Cake and Roses</div>

			<a  href="{{ url('/cake') }}" title="Bouton cake" class="cake">Cake</a>
			<a  href="{{ url('/rose') }}" title="Bouton rose" class="roses">Roses</a>
			
		</header><!-- /header -->

		<section class="container">
			<section class="info">
				<h1 id="info"> Qui sommes-nous ? </h1>
				<p> Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
				tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
				quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
				consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
				cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
				proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
			</section><!-- End Header -->

		</section>

@endsection