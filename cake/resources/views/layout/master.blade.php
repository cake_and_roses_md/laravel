<!DOCTYPE html>
<html lang="fr">
	<head>
		<title>Cake & Roses</title>

		<meta charset="UTF-8" />
		<link rel="stylesheet" href ="{{asset ('css/style.css')}}" >
		<meta name="description" content="
	 
				Vente de fleurs et de gateaux 

						" />

		<meta name="keywords" content="Fleurs, gateaux, cake, roses" />

		<link rel="icon" type="image/png" href="{{asset('favicon/favicon.png')}}"/>

	</head>

	<body>

        @yield('content')

        @include('layout.footer')

        <script src="https://use.typekit.net/zmm4zgm.js"></script>
		<script>try{Typekit.load({ async: true });}catch(e){}</script>


    </body>
</html>
