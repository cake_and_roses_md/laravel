@extends('layout/master')

@section('content')

		<header id="header" class="slider">
<?php $cpt = rand(1,6); ?>

<img class="img_header"src="{{ URL::to ('img/cake/cake_'.$cpt.'.jpg') }}" alt="image header">
	    

			<div class="logo">Logo Cake and Roses</div>

			<a  href="{{ url('cake/') }}#info" title="Bouton cake" class="cake">Cake</a>
			<a  href="{{ url('rose/') }}" title="Bouton rose" class="roses">Roses</a>
			
		</header><!-- /header -->

			<section class="produit">
			<a href="{{ url('rose/') }}" class="switch_categorie_f" title="changement de catégorie">direction fleurs</a>
				<h2 id='info'> Nos gâteaux</h2>

		<ul class="tag">
		
@foreach ($tags as $tag)
				<li><a href="#info" title="tag">{{ $tag->tagName}}</a></li>
@endforeach			    

		</ul>

				<div class="img_produit">
				<?php $cpt=0; ?> 
	@foreach ($img_cake as $img_cakes)	

		@if ($cpt==0)
		<div class="img_big">
			<img src="{{ URL::to ('img/cake/'.$img_cakes->imaName)}}" alt="{{$img_cakes->imaName}}">
		</div>
		@endif
		<?php $cpt++; ?>
		@if ($cpt>1 & $cpt<7)

		<div class="img_small">
			<img src="{{ URL::to ('img/cake/'.$img_cakes->imaName)}}" alt="{{$img_cakes->imaName}}">
		</div>
		@endif
	@endforeach		
					</div>
				</div>


				<div class="description">

					<h2>Description</h2>

					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
					tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
					quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
					consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
					cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
					proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>

				</div>

			</section>
			
@endsection

