@extends('layout/admin')
@section('content')

	@if (count($errors)>0)
	    <span>
	        <ul>
	            @foreach($errors->all() as $error)
	            <li>{{$error}}</li>
	            @endforeach
	        </ul>
	    </span>
	@endif

<?php
	$cpt_cake=0;
	$cpt_roses=0;
?>
	 @if ($cpt_cake>6 & $cpt_roses>6)
	  <H3>Vous ne pouvez plus ajouter de nouveaux tags.</H3>
	 @else
		<form method="post" action="{{ url('ajout-tag') }}" enctype="multipart/form-data">
				  <input type="hidden" name="{{ csrf_token() }}" value="">{{ csrf_field() }}

				<h3>Ajouter un Tag: </h3>

				<div class="{{ $errors->has('categorie')}}">
		        	<h2>Catégorie : </h2>
		        @if ($cpt_cake<6)
		        	<input class="admin_input" name="categorie" type="radio" value="0">Cakes<br />
		        @endif
		        @if ($cpt_roses<6)
		        	<input class="admin_input" name="categorie" type="radio" value="1">Roses<br />
		        @endif

				</div>
				<div class="{{ $errors->has('tagName')}}">
		        	<h2>Nom:</h2>

				  	<input type="text" name="tagName" placeholder="Nom du tag" value="{{old('tagName')}}" /><br /><br />

				</div>

			<input class="btn_hover" type="submit" value="Enregistrer le Tag" />

		</form>
	@endif

@endsection