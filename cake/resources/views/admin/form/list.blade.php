@extends('layout/admin')
@section('content')

<div class="content-admin">
<h3>Liste des articles :</h3>
	<table>
		<thead >
			<tr class="row thead_list_article_admin">
				<td class='col-4 name_article_admin'>Nom</td>
				<td class='col-2'></td>
				<td class='col-2'></td>
			</tr>
		</thead>
		<tbody>

			@foreach ($list_art as $list_arts)
				<tr class="row">
					<td class="col-4">{{ $list_arts->artName}}</td>
					<td class="col-2"><a href="#" class="_btn editarticles">édition</a></td>
					<td class="col-2"><button class="_btn danger deletearticles">Supprimer</button></td>
			    </tr>
			@endforeach	
			    
		</tbody>
	</table>
	
	<h3>Liste des tags :</h3>
	
	<table>
		<thead >
			<tr class="row thead_list_article_admin">
				<td class='col-4 name_article_admin'>Nom</td>
				<td class='col-4 categorie_article_admin'>Catégorie</td>
				<td class='col-2'></td>
				<td class='col-2'></td>
			</tr>
		</thead>
		<tbody>

			@foreach ($list_tag as $list_tags)
				<tr class="row">
					<td class="col-4">{{ $list_tags->tagName}}</td>
					<td class="col-2"><a href="#" class="_btn editarticles">édition</a></td>
					<td class="col-2"><button class="_btn danger deletearticles">Supprimer</button></td>
			    </tr>
			@endforeach	
		</tbody>
	</table>
	<script>
		$('button._btn.deletearticles').click(function() {
			let el = $(this);
			el.parent('td').parent('tr').children('td.delete-confirm').addClass('active')
		})
		$('button.cancelDelete._btn').click(function() {
			$(this).parent('.cancel').parent().parent('td.delete-confirm').removeClass('active')
		})
	</script>
	</div>
@endsection
	