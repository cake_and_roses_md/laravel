#------------------------------------------------------------
#        Script MySQL.
#------------------------------------------------------------


#------------------------------------------------------------
# Table: tag
#------------------------------------------------------------

CREATE TABLE tag(
        id           int (11) Auto_increment  NOT NULL ,
        tagCategorie TinyINT ,
        tagName      Varchar (255) ,
        PRIMARY KEY (id )
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: article
#------------------------------------------------------------

CREATE TABLE article(
        id             int (11) Auto_increment  NOT NULL ,
        artName        Varchar (255) ,
        artDescription Text ,
        artCategorie   TinyINT ,
        PRIMARY KEY (id )
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: note
#------------------------------------------------------------

CREATE TABLE note(
        id       int (11) Auto_increment  NOT NULL ,
        notValue TinyINT ,
        PRIMARY KEY (id )
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: image
#------------------------------------------------------------

CREATE TABLE image(
        id         int (11) Auto_increment  NOT NULL ,
        imaName    Varchar (255) ,
        id_article Int ,
        PRIMARY KEY (id )
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: definir
#------------------------------------------------------------

CREATE TABLE definir(
        id         Int NOT NULL ,
        id_article Int NOT NULL ,
        PRIMARY KEY (id ,id_article )
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: evaluer
#------------------------------------------------------------

CREATE TABLE evaluer(
        id      Int NOT NULL ,
        id_note Int NOT NULL ,
        PRIMARY KEY (id ,id_note )
)ENGINE=InnoDB;

ALTER TABLE image ADD CONSTRAINT FK_image_id_article FOREIGN KEY (id_article) REFERENCES article(id);
ALTER TABLE definir ADD CONSTRAINT FK_definir_id FOREIGN KEY (id) REFERENCES tag(id);
ALTER TABLE definir ADD CONSTRAINT FK_definir_id_article FOREIGN KEY (id_article) REFERENCES article(id);
ALTER TABLE evaluer ADD CONSTRAINT FK_evaluer_id FOREIGN KEY (id) REFERENCES article(id);
ALTER TABLE evaluer ADD CONSTRAINT FK_evaluer_id_note FOREIGN KEY (id_note) REFERENCES note(id);
