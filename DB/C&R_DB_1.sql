#------------------------------------------------------------
#        Script MySQL.
#------------------------------------------------------------


#------------------------------------------------------------
# Table: article
#------------------------------------------------------------

CREATE TABLE article(
        artID          int (11) Auto_increment  NOT NULL ,
        artName        Varchar (75) ,
        artDescription Varchar (500) ,
        PRIMARY KEY (artID )
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: tag
#------------------------------------------------------------

CREATE TABLE tag(
        tagID   int (11) Auto_increment  NOT NULL ,
        tagName Varchar (25) ,
        PRIMARY KEY (tagID )
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: note
#------------------------------------------------------------

CREATE TABLE note(
        noteID   int (11) Auto_increment  NOT NULL ,
        notValue TinyINT ,
        PRIMARY KEY (noteID )
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: image
#------------------------------------------------------------

CREATE TABLE image(
        imageID int (11) Auto_increment  NOT NULL ,
        imgName Varchar (150) ,
        artID   Int ,
        PRIMARY KEY (imageID )
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: definis
#------------------------------------------------------------

CREATE TABLE definis(
        tagID Int NOT NULL ,
        artID Int NOT NULL ,
        PRIMARY KEY (tagID ,artID )
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: evalue
#------------------------------------------------------------

CREATE TABLE evalue(
        artID  Int NOT NULL ,
        noteID Int NOT NULL ,
        PRIMARY KEY (artID ,noteID )
)ENGINE=InnoDB;

ALTER TABLE image ADD CONSTRAINT FK_image_artID FOREIGN KEY (artID) REFERENCES article(artID);
ALTER TABLE definis ADD CONSTRAINT FK_definis_tagID FOREIGN KEY (tagID) REFERENCES tag(tagID);
ALTER TABLE definis ADD CONSTRAINT FK_definis_artID FOREIGN KEY (artID) REFERENCES article(artID);
ALTER TABLE evalue ADD CONSTRAINT FK_evalue_artID FOREIGN KEY (artID) REFERENCES article(artID);
ALTER TABLE evalue ADD CONSTRAINT FK_evalue_noteID FOREIGN KEY (noteID) REFERENCES note(noteID);
