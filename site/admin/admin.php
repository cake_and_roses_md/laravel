<html lang="fr">
	<head>
		<meta charset="UTF-8" />
		<title>Administration | Cake&Roses</title>
		<link rel="stylesheet" href="css/style.css" />
	</head>
	<body class="admin-body">
	<div class="row container">
			<div class="col-3">
				<nav class="administration--nav">
					<ul>
						<li class="admin-nav-link">
							<a href="admin.php?o=1">Article</a>
						</li>
						<li class="admin-nav-link">
							<a href="admin.php?o=2">Tag</a>
						</li>
						<li class="admin-nav-link">
							<a href="admin.php?o=3">Liste</a>
						</li>
						<li class="admin-nav-link">
							<a class='logout' href="admin.php">Déconnexion</a>
						</li>
					</ul>
				</nav>
			</div>
<?php

if(!isset($_GET['o'])) {$section=0;}else {$section=$_GET['o'];}

	if($section==0) {
?>
<h1>Bienvenue Sur l'administration de Cake&Roses</h1>

<?php  
		}else if($section==1) { include 'form/ajout-article.php';
			}else if($section==2) { include 'form/ajout-tag.php';
				}else if($section==3) { include 'form/list.php';}			
?>
		
	</body>
</html>