<!DOCTYPE html>
<html lang="fr">
	<head>
		<title>Cake and Roses</title>

		<meta charset="UTF-8" />
		<link rel="stylesheet" href="assets/style/style.css" >
		<meta name="description" content="
	 
				Vente de fleurs et de gateaux 

						" />

		<meta name="keywords" content="Fleurs, gateaux, cake, roses" />
		<link rel="icon" type="image/png" href="assets/favicon/favicon.png"/>
		
		<script src="https://use.typekit.net/zmm4zgm.js"></script>
		<script>try{Typekit.load({ async: true });}catch(e){}</script>


	</head>

	<body>
	<div class="test"></div>
		
	
		<header id="header" class="slider">

			<div class="logo">Logo Cake and Roses</div>

			<a  href="#encre" title="Bouton cake" class="cake">Cake</a>
			<a  href="#encre" title="Bouton rose" class="roses">Roses</a>
			
		</header><!-- /header -->

		<section class="container">
		<a href="admin/admin.php" title="accès admin" class="admin">ADMIN</a>
			<div class="switch_categorie">Ceci est un Cupcake !</div>
			<section class="info">
				<h1 id="encre"> Qui sommes-nous ? </h1>
				<p> Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
				tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
				quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
				consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
				cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
				proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
			</section>

			<section class="produit">

				<h2> Nos fleurs | nos gâteaux</h2>

				<ul class="tag">
				<?php 
				for ($i=1; $i < 6 ; $i++) { ?>
					<li><a href="#" title="tag">TAG <?php echo $i;?></a></li>
				<?php
				}
				?>	
				</ul>

				<div class="img_produit">

					<div class="img_big">Ceci est la grosse img</div>
				<?php 
				for ($i=1; $i < 6 ; $i++) { ?>
					<div class="img_small">petite image N°<?php echo $i;?></div>
				<?php }?>
				</div>

				<div class="description">

					<h2>Description</h2>

					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
					tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
					quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
					consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
					cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
					proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>

				</div>

			</section>
			
		</section>

		<footer>

			<form action="#" method="get">

<!-- 				<label>Nom :</label> -->
				<input type="text" name="nom" placeholder="Votre nom"> 

				<!-- <label>Prénom :</label>  -->
				<input type="text" name="prenom" placeholder="Votre prénom">

			
				<br /><!-- 
				<label>Mail</label> -->
				<input type="mail" name="mail" placeholder="Votre mail">

				<!-- <label>Téléphone</label> -->
				<input type="tel" name="telephone" placeholder="Votre numéro de téléphone">

				<br />

<!-- 				<label>Sujet</label> -->
				<input class="sujet" type="text" name="sujet" placeholder="Sujet :">

				<br />

		<!-- 		<label>Message</label> -->
				<textarea name="message" placeholder="Votre message ..."></textarea>


				<input class="btn_envoyer" type="submit" value="Envoyer" name="envoyer">
			
			</form>
			<p> Laravel examen | Matthieu et Daniel | 2017  </p>

			<br />
			
		
		</footer>

		
	</body>


</html>
